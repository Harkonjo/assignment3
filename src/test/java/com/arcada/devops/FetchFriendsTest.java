package com.arcada.devops;

import static org.junit.Assert.*;

import org.junit.Test;


public class FetchFriendsTest
{
    @Test
    public void testFetchFriendsListReturnsNonNull() {
        // checks if the returned array is not null
        FetchFriends fetcher = new FetchFriends();

        String[] friendsList = fetcher.fetchFriendsList();
        assertNotNull(friendsList);
    }

    @Test
    public void testFetchFriendsListHandlesInvalidNames() {
        // checks that all the names contain alphabetic characters only
        FetchFriends fetcher = new FetchFriends();

        String[] friendsList = fetcher.fetchFriendsList();
        for (String friend : friendsList) {
            assertTrue(friend.matches("[A-Za-z]+"));
        }
    }

    @Test
    public void testFetchFriendsListPerformance() {
        // checks the performance of the function to fetch friends
        FetchFriends fetcher = new FetchFriends();

        long startTime = System.nanoTime();
        String[] friendsList = fetcher.fetchFriendsList();
        long endTime = System.nanoTime();

        long time = endTime - startTime;
        System.out.println("Time it took to fetch friends list: " + time + " nanoseconds");
    }

}
